# silvercat v1.0.4
<br>

## Bash script che crea un WEB Server in grado di servire una pagina HTML
<br>

#### Puoi usare questo per
- Ricerca e test
- Servire una singola pagina html custom come quella di esempio fornita
- Simulare installazioni di WEB Server e/o MITM e/o Cross-Site requests
- Honeypot o possibili disatri al tuo sistema (don't try this at home)

#### Come avviare il Server
- Lancia `./silverCat.sh` o con path assoluto in base a dove hai salvato lo script ed a dove sei posizionato
- `silverCat.sh` accetta il parametro `--mode` che permette di scegliere il modo di servire la pagina (`cmd` o `eof`).<br>Il valore `cmd` permette di renderizzare eventuali comandi inseriti nella pagina HTML<br>Il valore `eof` non renderizza i comandi ma crea un server più stabile (prova ad esempio a refreshare la pagina velocemente con `CTRL+R` sia con opzione `cmd` sia con opzione `eof`).
- `silverCat.sh` accetta il parametro `--port` che imposta la porta di ascolto
- Puoi passare `--response` per impostare il response code. Il default è `200` che setta il response `HTTP/1.1 200 OK`
- Se vuoi modifica `silverCat.sh` per settare gli header di risposta
- Se vuoi modifica o sostituisci `index.html` secondo necessità
- Se hai problemi di permessi prova a lanciare lo script o via `sudo` o con un utente con poteri di `root`

#### Note
- Questo progetto usa `netcat traditional`.<br>Assicurati che sia il tuo `nc` primario o modifica la variabile `pathNc`. Nota che se usi l'opzione `eof` non è necessario usare per forza `netcat traditional`
- Se usi l'opzione `cmd` non includere l'header `HContentLenght` trà gli header di risposta o impostagli un valore ragionevolmente grosso
- Nella pagina di esempio ho inserito tre comandi il cui output è iniettatto nella pagina stessa: `date` `figlet` e `fortune`<br>
`date` è presente in tutte le distribuzioni, `figlet` e `fortune` non sempre.
- La porta che apri fornirà solo la pagina che forzi qualsiasi sia la richiesta che venga effettuata. Non è lo scopo del progetto servire più pagine quindi non viene considerato un problema
- Il tutto è da utilizzare secondo tuo rischio e pericolo

#### Problemi o fastidi noti
- Usando `netcat traditional` non puoi stoppare il server con `CTRL+C`, devi killare il processo.
- Quando usi l'opzione `cmd` spesso non si riesce a visualizzare il codice sorgente della pagina con `CTRL+U`
- Non posso aggiungere più classi nei tag HTML perchè vengono renderizzate come attributi ... Bah .. Shitty things ...

#### To Do
- Renderizzare pagine diverse in base al response code


<br><br>
**CopyLeft n4d4s 2021-2022**
<br><br><br><br>

