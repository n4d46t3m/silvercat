#!/bin/bash

########################################################################
#
# silvercat.sh v1.0.4
#
#   n4d46t3m netcat 2021-2022 copyLeft or MIT
#
#          _  _       _ _  _       
#    _ __ | || |   __| | || |  ___ 
#   | '_ \| || |_ / _` | || |_/ __|
#   | | | |__   _| (_| |__   _\__ \
#   |_| |_|  |_|  \__,_|  |_| |___/                               
#    ____  _ _                 ____      _   
#   / ___|(_) |_   _____ _ __ / ___|__ _| |_ 
#   \___ \| | \ \ / / _ \ '__| |   / _` | __|
#    ___) | | |\ V /  __/ |  | |__| (_| | |_ 
#   |____/|_|_| \_/ \___|_|   \____\__,_|\__|
#                                         
# Start a very small webserver that can display only one big page
#
# NOTE !!! 
#   1) Before you run this, you must check pathNc and listeningPort 
#   2) BE SURE TO USE netcat traditional (you need -c option) !!!!
#   3) Only one (maybe big) HTML page is served
#   4) If You start this shit from cli, ctrl+c will NOT kill anything
#
########################################################################

########################################################################
# Setting some defaults
########################################################################

debugMode=1 # Set to 1 to activate DEBUG
pathNc="/usr/bin/nc.traditional" # Or nc or /usr/bin/nc or /usr/bin/nc.traditional or /usr/bin/nc.openbsd or some other netcat (read README.md)
RN="\r\n"
NN="\n\n"
ncMode="cmd"
listeningPort=6969
responseCode=200
response="HTTP/1.1 ${responseCode} OK"

########################################################################
# Managing user options
########################################################################

if [ $# -eq 0 ];then
    echo -e "No arguments supplied, using cmd mode, port ${listeningPort} and response ${response} as defaults\n"
else
    # Now I will NOT use either getopt or getopts ... RIP ... 
    userArgs=("$@")
    for userArg in "${userArgs[@]}";do
        currentOption=(${userArg//=/ })
        if [ "${currentOption[0]}" == "--help" ];then
            echo
            echo "NAME"
            echo -e "\tsilvercat"
            echo
            echo "SYNOPSIS"
            echo -e "\tsilvercat.sh [--help] [--mode] [--port] [--response]"
            echo
            echo "DESCRIPTION"
            echo -e "\tStart a very small webserver that can display only one big page"
            echo
            echo "OPTIONS"
            echo -e "\t--help"
            echo -e "\t\tOutput this help message and exit"
            echo
            echo -e "\t--mode=MODE"
            echo -e "\t\tSet netcat input mode, accepted MODE values are 'cmd' or 'eof'"
            echo
            echo -e "\t--port=PORTNUMBER"
            echo -e "\t\tSet listening port between 2 and 65534"
            echo
            echo -e "\t--response=CODE"
            echo -e "\t\tSet HTTP response code. EG: --response=500"
            echo
            exit 0
        elif [ "${currentOption[0]}" == "--mode" ];then 
            if [[ $debugMode == 1 ]];then
                echo "Detected ${userArg}"
            fi
            if [ "${currentOption[1]}" == "eof" ];then 
                ncMode="eof"
            elif [ "${currentOption[1]}" == "cmd" ];then 
                ncMode="cmd"
            else
                echo "Invalid value for --mode, using cmd as default"
            fi
        elif [ "${currentOption[0]}" == "--port" ];then 
            if [[ $debugMode == 1 ]];then
                echo "Detected ${userArg}"
            fi
            if [[ "${currentOption[1]}" -gt 1 && "${currentOption[1]}" -lt 65535 ]];then
                listeningPort=${currentOption[1]}
            else
                echo "Invalid value for --port, using ${responseCode} as default"
            fi
        elif [ "${currentOption[0]}" == "--response" ];then
            if [[ $debugMode == 1 ]];then
                echo "Detected ${userArg}"
            fi
            if [ "${currentOption[1]}" == 200 ];then response="HTTP/1.1 200 OK" 
            elif [ "${currentOption[1]}" == 204 ];then response="HTTP/1.1 204 No Content"
            elif [ "${currentOption[1]}" == 301 ];then response="HTTP/1.1 301 Moved Permanently"
            elif [ "${currentOption[1]}" == 302 ];then response="HTTP/1.1 302 Found"
            elif [ "${currentOption[1]}" == 307 ];then response="HTTP/1.1 307 Temporary Redirect"
            elif [ "${currentOption[1]}" == 308 ];then response="HTTP/1.1 308 Permanent Redirect"
            elif [ "${currentOption[1]}" == 400 ];then response="HTTP/1.1 400 Bad Request"
            elif [ "${currentOption[1]}" == 401 ];then response="HTTP/1.1 401 Unauthorized"
            elif [ "${currentOption[1]}" == 402 ];then response="HTTP/1.1 402 Payment Required"
            elif [ "${currentOption[1]}" == 403 ];then response="HTTP/1.1 403 Forbidden"
            elif [ "${currentOption[1]}" == 404 ];then response="HTTP/1.1 404 Not Found"
            elif [ "${currentOption[1]}" == 405 ];then response="HTTP/1.1 405 Method Not Allowed"
            elif [ "${currentOption[1]}" == 408 ];then response="HTTP/1.1 408 Request Timeout"
            elif [ "${currentOption[1]}" == 410 ];then response="HTTP/1.1 410 Gone"
            elif [ "${currentOption[1]}" == 415 ];then response="HTTP/1.1 415 Unsupported Media Type"
            elif [ "${currentOption[1]}" == 418 ];then response="HTTP/1.1 418 I'm a teapot"
            elif [ "${currentOption[1]}" == 429 ];then response="HTTP/1.1 429 Too Many Requests"
            elif [ "${currentOption[1]}" == 451 ];then response="HTTP/1.1 451 Unavailable For Legal Reasons"
            elif [ "${currentOption[1]}" == 500 ];then response="HTTP/1.1 500 Internal Server Error"
            elif [ "${currentOption[1]}" == 501 ];then response="HTTP/1.1 501 Not Implemented"
            elif [ "${currentOption[1]}" == 502 ];then response="HTTP/1.1 502 Bad Gateway"
            elif [ "${currentOption[1]}" == 503 ];then response="HTTP/1.1 503 Service Unavailable"
            elif [ "${currentOption[1]}" == 504 ];then response="HTTP/1.1 504 Gateway Timeout"
            elif [ "${currentOption[1]}" == 505 ];then response="HTTP/1.1 505 HTTP Version Not Supported"
            elif [ "${currentOption[1]}" == 506 ];then response="HTTP/1.1 506 Variant Also Negotiates"
            elif [ "${currentOption[1]}" == 507 ];then response="HTTP/1.1 507 Insufficient Storage"
            elif [ "${currentOption[1]}" == 508 ];then response="HTTP/1.1 508 Loop Detected"
            elif [ "${currentOption[1]}" == 510 ];then response="HTTP/1.1 510 Not Extended"
            elif [ "${currentOption[1]}" == 511 ];then response="HTTP/1.1 511 Network Authentication Required"
            else 
                echo "Invalid --response value, using ${responseCode} option as default"
                response="HTTP/1.1 200 OK"
            fi
        fi
    done
fi
if [[ $debugMode == 1 ]];then
    echo -e ">>>>>>>>> Dynamic vars <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
    echo "ncMode = ${ncMode}" # DEBUG
    echo "listeningPort = ${listeningPort}" # DEBUG
    echo "response = ${response}" # DEBUG
    echo
fi

########################################################################
# Working on served Page
########################################################################

pageToDraw="index.html"
# The page can contain html code, css, base64 images and javascript
#page=$(cat index.html) # This is shitty and easy and yes is fucking bugged ....
page=$(sed 's/;base64,\+/\\;base64,/g' ${pageToDraw}) # nc -c seem break response body if there are unescaped semicolons in some parts of the page
if [[ $debugMode == 1 ]];then
    echo -e ">>>>>>>>> RAW HTML PAGE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
    echo -e "${page}\n" # DEBUG
fi
contentLen=$(echo -en "${page}"|wc -c) # If you want you can use this with HContentLenght

########################################################################
# Setting Response Headers
########################################################################

hLocation="Location: http://127.0.0.1/newpage.html" # Use this with response 301 302 307 308
HRetryAfter="Retry-After: 10" # Use with response 301 429 501 503
HWWWAuthenticate="WWW-Authenticate: Basic realm=\"n4d46t3m Secrets\", charset=\"UTF-8\"" # Use with response 401
HConnection="Connection: Close" # or "Connection: Keep-Alive" if you want...
HContentType="Content-Type: text/html; charset=utf-8"
HContentLenght="Content-Length: ${contentLen}" # For now don't use it with cmd option or set it to a reasonable high value
HAccessControlAllowMethods="Access-Control-Allow-Methods: GET"
HContentLanguage="Content-Language: en"
HXPoweredBy="X-Powered-By: n4d4s"
HServer="Server: n4d4s"
# Add or remove response headers from here
headerJoined=$(cat<<EOF
${HConnection}
${HContentType}
${HAccessControlAllowMethods}
${HContentLanguage}
${HXPoweredBy}
${HServer}
EOF
)
if [[ $debugMode == 1 ]];then
    echo -e "\n>>>>>>>>> RESPONSE HEADERS THAT WILL SENT TO CLIENT <<<<<<<<<\n"
    echo -e "${response}${RN}${headerJoined}${NN}" # DEBUG
    echo -e "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
fi

########################################################################
# Starting the server
########################################################################

echo -e "You can reach served page at http://127.0.0.1:${listeningPort}/\n"
while true ; do
    if [ "$ncMode" == "cmd" ];then
        $pathNc -q 1 -l -p ${listeningPort} -c "echo \"${response}${RN}${headerJoined}${NN}${page}\""
    elif [ "$ncMode" == "eof" ];then
        $pathNc -q 1 -l -p ${listeningPort} <<EOF
${response}
${headerJoined}

${page}
EOF
    fi
done
